const fs = require("fs")

const path = require('path');

// Question 1
var data = {
    "max": {
        colors: ['Orange', 'Red']
    }
}

let stringifyData = JSON.stringify(data)

fs.writeFile(path.join(__dirname, "./data.json"), stringifyData, (err) => {
    if (err) {
        console.log(err.message)
    } else {
        console.log("file created")
    }
})

// Question 2

fs.readFile("/home/satish/Desktop/practice/afterIpl/data.json", "utf8", (err, data) => {
    if (err) {
        console.log(err.message)
    } else {
        const newData = JSON.parse(data)
        // console.log(data, newData);
        fs.writeFile("/home/satish/Desktop/practice/afterIpl/output/data.json", JSON.stringify(newData), "utf8", (err) => {
            if (err) {
                console.log(err.message)
            } else {
                console.log("data add to JSON file")
            }
        })
    }
})

setTimeout(() => {
    fs.unlink('/home/satish/Desktop/practice/afterIpl/data.json', (err) => {
        if (err) {
            console.log(err.message);
        }
    
        console.log("File is deleted.");
    })
}, 1000);



// // Question 3.

function addNameAndFavcolor(data, name, favColors) {
    data[name] = { colors: favColors }
    // console.log(data, newData);
    return data;
}

fs.readFile(path.join(__dirname, "./output/data.json"), (err, data) => {
    if (err) {
        console.log(err.message);
    } else {
        const newData = addNameAndFavcolor(JSON.parse(data), "Madhu", ["black", "red"]);
        fs.writeFile(path.join(__dirname, "./output/data.json"), JSON.stringify(newData), (err) => {
            if (err) {
                console.log(err);
            } else {
                console.log("Add data to JSON");
            }
        })
    }
})

// // // Question 4 

function addFavColor(data, name, hobby) {
        data[name].hobby = hobby
    
    return data
}


fs.readFile(path.join(__dirname, "./output/data.json"), (err, data) => {
    if (err) {
        console.log(err.message);
    } else {
        const newData = addFavColor(JSON.parse(data), "Madhu", ["Playing Games", "Watching Movies"]);
        fs.writeFile(path.join(__dirname, "./output/data.json"), JSON.stringify(newData), (err) => {
            if (err) {
                console.log(err.message)
            } else {
                console.log("Add data to JSON")
            }
        })
    }
})